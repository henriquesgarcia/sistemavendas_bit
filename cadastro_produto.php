
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Cadastro de Cliente</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
	<div class='container'>
		<fieldset>
			<legend><h1>Formulário - Cadastro de Produto</h1></legend>
			
			<form action="action_produto.php" method="post" id='form-contato' enctype='multipart/form-data'>

				<div class="form-group">
			      <label for="nome">Codigo do Cliente</label>
			      <input type="text" class="form-control" id="codigo_cliente" name="codigo_cliente" placeholder="Infome o Codigo do Cilente">
			      <span class='msg-erro msg-nome_produto'></span>
			    </div>


			    <div class="form-group">
			      <label for="nome">Nome do Produto</label>
			      <input type="text" class="form-control" id="nome_produto" name="nome_produto" placeholder="Infome o Nome do Produto">
			      <span class='msg-erro msg-nome_produto'></span>
			    </div>

			    <div class="form-group">
			      <label for="nome">Valor Produto</label>
			      <input type="text" class="form-control" id="valor_produto" name="valor_produto" placeholder="Infome o Valor do Produto">
			      
			    </div>

			    <div class="form-group">
			      <label for="nome">Quantidade</label>
			      <input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Infome a Quantidade de vendas do Produto">
			      
			    </div>

			    <div class="form-group">
			      <label for="status">Baixa Pagamento</label>
			      <select class="form-control" name="baixa_pagamento" id="baixa_pagamento">
				    <option value="">Selecione o Status</option>
				    <option value="Pago">Pago</option>
				    <option value="Inadiplente">Inadiplente</option>
				  </select>
				  
			    </div>

			    <input type="hidden" name="acao" value="incluir">
			    <button type="submit" class="btn btn-primary" id='botao'> 
			      Gravar
			    </button>
			    <a href='index.php' class="btn btn-danger">Cancelar</a>
			     <a href='index.php' class="btn btn-danger">Verificar dados dos clientes</a>
			</form>
		</fieldset>
	</div>
	<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>