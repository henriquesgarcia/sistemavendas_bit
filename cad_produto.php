
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Cadastro de Cliente</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>

    <!-- Barra de Navegação -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="index.php">Controle de Vendas</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Caixa<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cad_venda.php">Vendas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_cliente.php">Clientes</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listagem_produto.php">Produtos</a>
                </li>
            </ul>
        </div>
    </nav>



	<div class='container'>
		<fieldset>
			<legend><h1>Formulário - Cadastro de Produtos</h1></legend>
			
			<form action="action_cad_produto.php" method="post" id='form-contato' enctype='multipart/form-data'>



			    <div class="form-group">
			      <label for="nome">Nome do Produto</label>
			      <input type="text" class="form-control" id="nome_produto" name="nome_produto" placeholder="Infome o Nome do Produto">
			      <span class='msg-erro msg-nome_produto'></span>
			    </div>

			    <div class="form-group">
			      <label for="nome">Valor Produto</label>
			      <input type="text" class="form-control" id="valor_produto" name="valor_produto" placeholder="Infome o Valor do Produto">
			      
			    </div>

			    <input type="hidden" name="acao" value="incluir">
			    <button type="submit" class="btn btn-primary" id='botao'> 
			      Gravar
			    </button>
			    <a href='index.php' class="btn btn-danger">Cancelar</a>
			     
			</form>
		</fieldset>
	</div>
	<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>