<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Sistema de Cadastro</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
	<div class='container box-mensagem-crud'>
		<?php 
		require 'conexao.php';

		// Atribui uma conexão PDO
		$conexao = conexao::getInstance();

		// Recebe os dados enviados pela submissão
		$acao  = (isset($_POST['acao'])) ? $_POST['acao'] : '';
		$id    = (isset($_POST['id'])) ? $_POST['id'] : '';
		$nome_produto  = (isset($_POST['nome_produto'])) ? $_POST['nome_produto'] : '';
		$valor_produto  = (isset($_POST['valor_produto'])) ? $_POST['valor_produto'] : '';
		
		// Valida os dados recebidos
		$mensagem = '';
		if ($acao == 'editar' && $id == ''):
		    $mensagem .= '<li>ID do registros desconhecido.</li>';
	    endif;

	    // Se for ação diferente de excluir valida os dados obrigatórios
	    if ($acao != 'excluir'):


			if ($nome_produto == '' || strlen($nome_produto) < 3):
				$mensagem .= '<li>Favor preencher dados sobre o produto.</li>';
		    endif;

		    if ($valor_produto == ''):
			   $mensagem .= '<li>Favor preencher valor do produto.</li>';
			endif;

			if ($mensagem != ''):
				$mensagem = '<ul>' . $mensagem . '</ul>';
				echo "<div class='alert alert-danger' role='alert'>".$mensagem."</div> ";
				exit;
			endif;

			// Constrói a data no formato ANSI yyyy/mm/dd
		
		endif;



		// Verifica se foi solicitada a inclusão de dados
		if ($acao == 'incluir'):

			
			    
 

			$sql = 'INSERT INTO tab_cad_produtos ( nome_produto, valor_produto)
							   VALUES( :nome_produto, :valor_produto)';

			$stm = $conexao->prepare($sql);
			$stm->bindValue(':nome_produto', $nome_produto);
			$stm->bindValue(':valor_produto', $valor_produto);
			$retorno = $stm->execute();

			if ($retorno):
				echo "<div class='alert alert-success' role='alert'>Registro inserido com sucesso, aguarde você está sendo redirecionado ...</div> ";
		    else:
		    	echo "<div class='alert alert-danger' role='alert'>Erro ao inserir registro!</div> ";
			endif;

			echo "<meta http-equiv=refresh content='3;URL=index.php'>";
		endif;


		// Verifica se foi solicitada a edição de dados
		if ($acao == 'editar'):

			

			     // Validamos se a extensão do arquivo é aceita
			    
			$sql = 'UPDATE tab_cad_produtos SET  nome_produto=:nome_produto, valor_produto=:valor_produto';
			$sql .= 'WHERE id = :id';

			$stm = $conexao->prepare($sql);
			$stm->bindValue(':nome_produto', $nome_produto);
			$stm->bindValue(':valor_produto', $valor_produto);
			$retorno = $stm->execute();
			$stm->bindValue(':id', $id);
			$retorno = $stm->execute();

			if ($retorno):
				echo "<div class='alert alert-success' role='alert'>Registro editado com sucesso, aguarde você está sendo redirecionado ...</div> ";
		    else:
		    	echo "<div class='alert alert-danger' role='alert'>Erro ao editar registro!</div> ";
			endif;

			echo "<meta http-equiv=refresh content='3;URL=index.php'>";
		endif;


		// Verifica se foi solicitada a exclusão dos dados
		if ($acao == 'excluir'):

			

			// Exclui o registro do banco de dados
			$sql = 'DELETE FROM tab_cad_produtos WHERE id = :id';
			$stm = $conexao->prepare($sql);
			$stm->bindValue(':id', $id);
			$retorno = $stm->execute();

			if ($retorno):
				echo "<div class='alert alert-success' role='alert'>Registro excluído com sucesso, aguarde você está sendo redirecionado ...</div> ";
		    else:
		    	echo "<div class='alert alert-danger' role='alert'>Erro ao excluir registro!</div> ";
			endif;

			echo "<meta http-equiv=refresh content='3;URL=index.php'>";
		endif;
		?>

	</div>
</body>
</html>