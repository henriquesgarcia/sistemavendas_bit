<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Sistema de Cadastro</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
	<div class='container box-mensagem-crud'>
		<?php 
		require 'conexao.php';

		// Atribui uma conexão PDO
		$conexao = conexao::getInstance();

		// Recebe os dados enviados pela submissão
		$acao  = (isset($_POST['acao'])) ? $_POST['acao'] : '';
		$id    = (isset($_POST['id'])) ? $_POST['id'] : '';
		$codigo_cliente  = (isset($_POST['codigo_cliente'])) ? $_POST['codigo_cliente'] : '';
		$codigo_produto  = (isset($_POST['codigo_produto'])) ? $_POST['codigo_produto'] : '';
		$quantidade  = (isset($_POST['quantidade'])) ? $_POST['quantidade'] : '';
		$baixa_pagamento  = (isset($_POST['baixa_pagamento'])) ? $_POST['baixa_pagamento'] : '';
		
		// Valida os dados recebidos
		$mensagem = '';
		if ($acao == 'editar' && $id == ''):
		    $mensagem .= '<li>ID do registros desconhecido.</li>';
	    endif;

	    // Se for ação diferente de excluir valida os dados obrigatórios
	    if ($acao != 'excluir'):


			if ($codigo_cliente == '' ):
				$mensagem .= '<li>Favor preencher codigo do cliente.</li>';
		    endif;

		    if ($codigo_produto == ''):
			   $mensagem .= '<li>Favor preencher codigo do produto.</li>';
			endif;

			if ($quantidade == ''):
			   $mensagem .= '<li>Favor preencher a quantidade de itens.</li>';
			endif;

			if ($baixa_pagamento == ''):
			   $mensagem .= '<li>Favor selecionar dados da venda.</li>';
			endif;

			if ($mensagem != ''):
				$mensagem = '<ul>' . $mensagem . '</ul>';
				echo "<div class='alert alert-danger' role='alert'>".$mensagem."</div> ";
				exit;
			endif;

			// Constrói a data no formato ANSI yyyy/mm/dd
		
		endif;



		// Verifica se foi solicitada a inclusão de dados
		if ($acao == 'incluir'):

			
			    
 

			$sql = 'INSERT INTO tab_vendas ( codigo_cliente, codigo_produto,quantidade,baixa_pagamento)
							   VALUES( :codigo_cliente, :codigo_produto,:quantidade,:baixa_pagamento)';

			$stm = $conexao->prepare($sql);
			$stm->bindValue(':codigo_cliente', $codigo_cliente);
			$stm->bindValue(':codigo_produto', $codigo_produto);
			$stm->bindValue(':quantidade', $quantidade);
			$stm->bindValue(':baixa_pagamento', $baixa_pagamento);
			$retorno = $stm->execute();

			if ($retorno):
				echo "<div class='alert alert-success' role='alert'>Registro inserido com sucesso, aguarde você está sendo redirecionado ...</div> ";
		    else:
		    	echo "<div class='alert alert-danger' role='alert'>Erro ao inserir registro!</div> ";
			endif;

			echo "<meta http-equiv=refresh content='3;URL=index.php'>";
		endif;


		// Verifica se foi solicitada a edição de dados
		if ($acao == 'editar'):

			

			     // Validamos se a extensão do arquivo é aceita
			    
			$sql = 'UPDATE tab_vendas SET  codigo_cliente=:codigo_cliente, codigo_produto=:codigo_produto,quantidade=:quantidade, baixa_pagamento=:baixa_pagamento';
			$sql .= 'WHERE id = :id';

			$stm = $conexao->prepare($sql);
			$stm->bindValue(':codigo_cliente', $codigo_cliente);
			$stm->bindValue(':codigo_produto', $codigo_produto);
			$stm->bindValue(':codigo_cliente', $quantidade);
			$stm->bindValue(':codigo_produto', $baixa_pagamento);
			$retorno = $stm->execute();
			$stm->bindValue(':id', $id);
			$retorno = $stm->execute();

			if ($retorno):
				echo "<div class='alert alert-success' role='alert'>Registro editado com sucesso, aguarde você está sendo redirecionado ...</div> ";
		    else:
		    	echo "<div class='alert alert-danger' role='alert'>Erro ao editar registro!</div> ";
			endif;

			echo "<meta http-equiv=refresh content='3;URL=index.php'>";
		endif;


		// Verifica se foi solicitada a exclusão dos dados
		if ($acao == 'excluir'):

			

			// Exclui o registro do banco de dados
			$sql = 'DELETE FROM tab_vendas WHERE id = :id';
			$stm = $conexao->prepare($sql);
			$stm->bindValue(':id', $id);
			$retorno = $stm->execute();

			if ($retorno):
				echo "<div class='alert alert-success' role='alert'>Registro excluído com sucesso, aguarde você está sendo redirecionado ...</div> ";
		    else:
		    	echo "<div class='alert alert-danger' role='alert'>Erro ao excluir registro!</div> ";
			endif;

			echo "<meta http-equiv=refresh content='3;URL=index.php'>";
		endif;
		?>

	</div>
</body>
</html>