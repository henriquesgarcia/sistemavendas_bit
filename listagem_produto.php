<?php
require 'conexao.php';

// Recebe o termo de pesquisa se existir
$termo7 = (isset($_GET['termo7'])) ? $_GET['termo7'] : '';

// Verifica se o termo de pesquisa está vazio, se estiver executa uma consulta completa
if (empty($termo7)):

	$conexao7 = conexao::getInstance();
	$sql7 = 'SELECT id, nome_produto, valor_produto FROM tab_cad_produtos';
	$stm7 = $conexao7->prepare($sql7);
	$stm7->execute();
	$produtos = $stm7->fetchAll(PDO::FETCH_OBJ);

else:

	// Executa uma consulta baseada no termo de pesquisa passado como parâmetro
	$conexao7 = conexao::getInstance();
	$sql7 = 'SELECT id, nome_produto, valor_produto FROM tab_cad_produtos WHERE nome_produto LIKE :nome_produto OR valor_produto LIKE :valor_produto';
	$stm7 = $conexao->prepare($sql7);
	$stm7->bindValue(':nome_produto', $termo.'%');
	$stm7->bindValue(':valor_produto', $termo.'%');
	$stm7->execute();
	$produtos = $stm7->fetchAll(PDO::FETCH_OBJ);

endif;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Listagem de Produtos</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>

    <!-- Barra de Navegação -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="index.php">Controle de Vendas</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Caixa<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cad_venda.php">Vendas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_cliente.php">Clientes</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listagem_produto.php">Produtos</a>
                </li>
            </ul>
        </div>
    </nav>



	<div class='container'>
		<fieldset>

			<!-- Cabeçalho da Listagem -->
			<legend><h1>Listagem de Produtos</h1></legend>

			<!-- Formulário de Pesquisa -->
			<form action="" method="get" id='form-contato' class="form-horizontal col-md-10">
				<label class="col-md-2 control-label" for="termo7">Pesquisar</label>
				<div class='col-md-7'>
			    	<input type="text" class="form-control" id="termo7" name="termo7" placeholder="Infome o Nome do produto  ou valor do produto">
				</div>
			    <button type="submit" class="btn btn-primary">Pesquisar</button>
			    <a href='index.php' class="btn btn-primary">Ver Todos</a>
			    <a href='cad_produto.php' class="btn btn-primary">Cadastrar Produtos</a>
			    <a href='cad_venda.php' class="btn btn-primary">Cadastrar Vendas</a>
			    <a href='index.php' class="btn btn-primary">Inicio</a>
			</form>

			<!-- Link para página de cadastro -->

			<a href='cadastro.php' class="btn btn-success pull-right">Cadastrar Cliente</a>
			<div class='clearfix'></div>

			<?php if(!empty($produtos)):?>

				<!-- Tabela de Clientes -->
				<table class="table table-striped">
					<tr class='active'>
						<th>Codigo Produto</th>
						<th>Nome Produto</th>
						<th>Valor Produto</th>
						<th>Ação</th>
					</tr>
					<?php foreach($produtos as $produto):?>
						<tr>
							<td><?=$produto->id?></td>
							<td><?=$produto->nome_produto?></td>
							<td><?=$produto->valor_produto?></td>
							
							<td>
								
								<a href='javascript:void(0)' class="btn btn-danger link_exclusao" rel="<?=$produto->id?>">Excluir Produto</a>
							</td>
						</tr>	
					<?php endforeach;?>
				</table>

			<?php else: ?>

				<!-- Mensagem caso não exista clientes ou não encontrado  -->
				<h3 class="text-center text-primary">Não existem produtos cadastrados!</h3>
			<?php endif; ?>
		</fieldset>
	</div>
	<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>