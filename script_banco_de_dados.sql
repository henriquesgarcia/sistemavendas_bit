CREATE DATABASE IF NOT EXISTS db_granja;

USE db_granja;

CREATE TABLE tab_clientes(
	id integer auto_increment primary key,
	nome varchar(100),
	cpf varchar(20),
	email varchar(50),
	telefone varchar(20),
	celular varchar(20),
	data_nascimento date,
	data_cadastro timestamp default CURRENT_TIMESTAMP,
	data_alteracao timestamp
);




CREATE TABLE tab_cad_produtos(
	id integer auto_increment primary key,
	nome_produto varchar(20),
    valor_produto varchar(20),
	data_cadastro timestamp default CURRENT_TIMESTAMP,
	data_alteracao timestamp
);


CREATE TABLE tab_vendas(
	id integer auto_increment primary key,
    codigo_cliente varchar(100),
	codigo_produto varchar(100),
	quantidade varchar(50),
	baixa_pagamento varchar(20),
	data_cadastro timestamp default CURRENT_TIMESTAMP,
	data_alteracao timestamp
);
