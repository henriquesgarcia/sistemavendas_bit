<?php
require 'conexao.php';

// Recebe o termo de pesquisa se existir
$termo8 = (isset($_GET['termo8'])) ? $_GET['termo8'] : '';

// Verifica se o termo de pesquisa está vazio, se estiver executa uma consulta completa
if (empty($termo8)):

	$conexao8 = conexao::getInstance();
	$sql8 = 'SELECT id, nome, cpf, email, celular FROM tab_clientes';
	$stm8 = $conexao8->prepare($sql8);
	$stm8->execute();
	$clientes = $stm8->fetchAll(PDO::FETCH_OBJ);

else:

	// Executa uma consulta baseada no termo de pesquisa passado como parâmetro
	$conexao8 = conexao::getInstance();
	$sql8 = 'SELECT id, nome, cpf, email, celular FROM tab_clientes WHERE nome LIKE :nome OR cpf LIKE :cpf';
	$stm8 = $conexao->prepare($sql8);
	$stm8->bindValue(':nome', $termo.'%');
	$stm8->bindValue(':cpf', $termo.'%');
	$stm8->execute();
	$clientes = $stm8->fetchAll(PDO::FETCH_OBJ);

endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
	<title>Listagem de Clientes - Controle de Vendas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>

    <!-- Barra de Navegação -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="index.php">Controle de Vendas</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Caixa<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cad_venda.php">Vendas</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listagem_cliente.php">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_produto.php">Produtos</a>
                </li>
            </ul>
        </div>
    </nav>


    <div class="col-lg-12">
        <div class='container-fluid'>

            <fieldset>

                <!-- Cabeçalho da Listagem -->
                <h1>Clientes Cadastrados</h1>

                <!-- Formulário de Pesquisa -->
                <form action="" method="get" id='form-contato' class="form-horizontal">

                    <label class="control-label" for="termo8">Pesquisar</label>
                    <input type="text" class="form-control" id="termo8" name="termo8" placeholder="Infome o Nome ou CPF">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                    <a href='index.php' class="btn btn-primary">Ver Todos</a>
                </form>

                <!-- Link para página de cadastro -->
                <a href='cadastro.php' class="btn btn-success float-right">Cadastrar Cliente</a>
                <div class='clearfix'></div>

                <?php if(!empty($clientes)):?>

                    <!-- Tabela de Clientes -->
                    <table class="table table-responsive-lg">
                        <thead class="thead-light">
                        <tr class='active'>
                            <th>Cód.</th>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>E-mail</th>
                            <th>Celular</th>
                            <th>Ação</th>
                        </tr>
                        </thead>
                        <?php foreach($clientes as $cliente):?>
                        <tbody class="">
                        <tr>
                            <td><?=$cliente->id?></td>
                            <td><?=$cliente->nome?></td>
                            <td><?=$cliente->cpf?></td>
                            <td><?=$cliente->email?></td>
                            <td><?=$cliente->celular?></td>
                            <td>
                                <a href='editar.php?id=<?=$cliente->id?>' class="btn btn-primary"><strong>Editar</strong></a>
                                <a href='javascript:void(0)' class="btn text-danger link_exclusao" rel="<?=$cliente->id?>"><strong>Excluir</strong></a>
                            </td>
                        </tr>
                        </tbody>
                        <?php endforeach;?>
                    </table>

                <?php else: ?>

                <!-- Mensagem caso não exista clientes ou não encontrado  -->
                <h3 class="text-center text-primary">Não existem clientes cadastrados!</h3>
                <?php endif; ?>
            </fieldset>
        </div>
    </div>

    <hr>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>