<?php
require 'conexao.php';
$conexao = conexao::getInstance();
$sql = 'SELECT sum(valor_produto * quantidade) AS total FROM tab_cad_produtos join tab_vendas' ;
$stm = $conexao->prepare($sql);
$stm->execute();
$relatorios = $stm->fetch(PDO::FETCH_OBJ);


$conexao = conexao::getInstance();
$sql2 = 'SELECT sum(valor_produto * quantidade) AS total FROM tab_cad_produtos join tab_vendas on tab_cad_produtos.id = tab_vendas.codigo_cliente where (tab_vendas.baixa_pagamento = "Inadiplente") ';    
$stm2 = $conexao->prepare($sql2);
$stm2->execute();
$clientes_inadiplentes = $stm2->fetch(PDO::FETCH_OBJ);


$conexao = conexao::getInstance();
$sql3 = 'SELECT sum(valor_produto * quantidade) AS total FROM tab_cad_produtos join tab_vendas on tab_cad_produtos.id = tab_vendas.codigo_cliente where (tab_vendas.baixa_pagamento = "Pago") ';  
$stm3 = $conexao->prepare($sql3);
$stm3->execute();
$clientes_adiplentes = $stm3->fetch(PDO::FETCH_OBJ);

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
	<title>Caixa - Controle de Vendas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>

    <!-- Barra de Navegação -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="index.php">Controle de Vendas</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Caixa<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cad_venda.php">Vendas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_cliente.php">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_produto.php">Produtos</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="col-lg-12">
        <div class='container-fluid'>

            <fieldset>
                <h1 class="content-center">Caixa</h1>

                <div class="container-fluid content-center">


                    <div class="row">
                        <div class="col-md-6">

                            <!-- Detalhes do Caixa - TOTAL DE VENDAS -->
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Total de Vendas</h4>
                                    <p class="card-text display-3 text-center">
                                        <span class="real-symbol">R$</span>
                                        <?php foreach($relatorios as $relatorio):?>
                                            <strong><?php print_r($relatorio);?></strong>
                                        <?php endforeach;?>
                                    </p>
                                    <button type="button" class="btn btn-info float-right text-uppercase" href="#"><strong>Vendas Realizadas</strong></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">

                            <!-- Detalhes do Caixa - TOTAL A RECEBER -->
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Total a Receber</h4>
                                    <p class="card-text display-3 text-center">
                                        <span class="real-symbol">R$</span>
                                        <?php foreach($clientes_inadiplentes as $clientes_inadiplente):?>
                                            <strong><?php print_r($clientes_inadiplente);?></strong>
                                        <?php endforeach;?>
                                    </p>
                                    <button type="button" class="btn btn-info float-right text-uppercase" href="#"><strong>Lista de Inadimplentes</strong></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">

                            <!-- Detalhes do Caixa - TOTAL EM CAIXA -->
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Total em Caixa</h4>
                                    <p class="card-text display-3 text-center">
                                        <span class="real-symbol">R$</span>
                                        <?php foreach($clientes_adiplentes as $clientes_adiplente):?>
                                            <strong><?php print_r($clientes_adiplente);?></strong>
                                        <?php endforeach;?>
                                    </p>
                                    <button type="button" class="btn btn-info float-right text-uppercase" href="#"><strong>Vendas Realizadas</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </fieldset>
        </div>
	</div>

    <hr>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>