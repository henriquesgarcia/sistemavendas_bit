
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
	<title>Cadastro de Venda</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>

    <!-- Barra de Navegação -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="index.php">Controle de Vendas</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Caixa<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="cad_venda.php">Vendas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_cliente.php">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listagem_produto.php">Produtos</a>
                </li>
            </ul>
        </div>
    </nav>



	<div class='container'>
		<fieldset>
			<legend><h1>Formulário - Cadastro de Vendas</h1></legend>
			
			<form action="action_cad_venda.php" method="post" id='form-contato' enctype='multipart/form-data'>

				<div class="form-group">
			      <label for="nome">Codigo do Cliente</label>
			      <input type="text" class="form-control" id="codigo_cliente" name="codigo_cliente" placeholder="Infome o Codigo do Cilente">
			      <span class='msg-erro msg-nome_produto'></span>
			    </div>


			    <div class="form-group">
			      <label for="nome">Codigo do Produto</label>
			      <input type="text" class="form-control" id="codigo_produto" name="codigo_produto" placeholder="Infome o Codigo do Produto">
			      <span class='msg-erro msg-nome_produto'></span>
			    </div>

			    <div class="form-group">
			      <label for="nome">Quantidade</label>
			      <input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Infome a Quantidade de vendas do Produto">
			      
			    </div>

			    <div class="form-group">
			      <label for="status">Baixa Pagamento</label>
			      <select class="form-control" name="baixa_pagamento" id="baixa_pagamento">
				    <option value="">Selecione o Status do pagamento</option>
				    <option value="Pago">Pago</option>
				    <option value="Inadiplente">Inadimplente</option>
				  </select>
				  
			    </div>

			    <input type="hidden" name="acao" value="incluir">
			    <button type="submit" class="btn btn-primary" id='botao'> 
			      Gravar
			    </button>
			    <a href='index.php' class="btn btn-danger">Cancelar</a>
			     <a href='listagem_cliente.php' class="btn btn-danger">Dados dos clientes</a>
			     <a href='listagem_produto.php' class="btn btn-danger">Dados Produtos</a>
			</form>
		</fieldset>
	</div>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>